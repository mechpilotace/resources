# Resourses 

This is a document for managing resources, guides, interesting links, and other information.  
General Breakdown will be by subject and can be subdivided as what makes sense to me. 

## Computer Science

### Linux

#### Command Line 
* [The Linux Command Line 5th Web Edition](http://www.linuxcommand.org/)

#### Utils

##### Vim
* <http://vimsheet.com/>
* ``$ vimtutor``
##### tmux
* [The Tao of tmux](https://leanpub.com/the-tao-of-tmux/read)
* [TMUX FOR MERE MORTALS](https://zserge.com/posts/tmux/)


##### git
* [gitlab guide](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
* [git pro book](https://git-scm.com/book/en/v2)
* [Official Docummentation](https://git-scm.com/)

##### mpv
* [Keyboard Shortcuts](https://cheatography.com/someone/cheat-sheets/mpv-media-player/)

##### irc
###### Clients
* [WeeChat Documentation](https://weechat.org/doc/)
* [WeeChat Quickstart](https://weechat.org/files/doc/stable/weechat_quickstart.en.html)
* <https://tldp.org/HOWTO/IRC/beginners.html>
* <https://opensource.com/article/16/6/irc-quickstart-guide>
* <http://www.ircbeginner.com/ircinfo/ircc-commands.html>
* [linode - weechat guide](https://www.linode.com/docs/applications/messaging/using-weechat-for-irc/)

###### Servers
* <https://freenode.net/>
* [Freenode KB - Registration](https://freenode.net/kb/answer/registration)
* [IRC Bouncer Bot - ZNC](https://prgmr.com/blog/2020/06/23/setting-up-irc-bouncer.html)
* <https://www.inspircd.org/>

### Windows

#### WMI
* [WMI: Basic Syntax and Querying](https://petri.com/command-line-wmi-part-1)

#### Powershell
* [Powershell vids from - Adam Bertram "Powershell for Sysadmins"](https://www.youtube.com/watch?v=nBmX4WPGz8k&list=PLG000KXfScRrzKBPbCLbeENatW-RNliq_)

#### Logging
* [Solarwinds - Ultimate logging guide](https://www.loggly.com/ultimate-guide/windows-logging-basics/)

### macOS

#### Homebrew
* <https://brew.sh/> - Adding *nix like package managment to macOS

### Programing / Languages

#### Markdown

##### MD-Programs
* <https://caret.io>
* <https://zettlr.com>
* <https://typora.io>
* <https://hackmd.io> - Collaborative Markdown Editor

##### Syntax - Guides
* <https://www.markdownguide.org/>
* [Adam-P Guide](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#links) 

##### Pandoc

#### C
* <https://www.learn-c.org/>
* C Programming Language 2nd Edition - ISBN: 978-0131103627
* [C Programming E-book](http://www2.cs.uregina.ca/~hilder/cs833/Other%20Reference%20Materials/The%20C%20Programming%20Language.pdf)
* [Caleb Curry - Playlist](https://www.youtube.com/playlist?list=PL_c9BZzLwBRKKqOc9TJz1pP0ASrxLMtp2)

#### Bash
* [The Linux Command Line 5th Web Edition](http://www.linuxcommand.org/)
* [Google Formatting Standards](https://google.github.io/styleguide/shellguide.html)
* <https://www.shellcheck.net/>
* [The Bash Hackers Wiki](https://wiki.bash-hackers.org/start)

#### Python

#### Web Development
* [Mozilla JavaScript Reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)
* [Mozilla CSS Reference](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference#Keyword_index)
* [Mozilla HTML elements Reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)
* [API Cocumentation - devdocs.io](https://devdocs.io/)
* [Colors and Palettes](https://coolors.co/)
* [Web Dev Roadmap](https://github.com/kamranahmedse/developer-roadmap)
* [HTML - Living Standard](https://html.spec.whatwg.org/)
* [HTML - Can I use?](https://caniuse.com/#search=%3Cp%3E)

### Raspberry Pi
* [pizerow usb ssh guide](https://desertbot.io/blog/ssh-into-pi-zero-over-usb)
* [Learn Kernel Development on Raspberry Pi](https://s-matyukevich.github.io/raspberry-pi-os/)

### BSD
* [FreeBSD Handbook](https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook)
* <https://openbsd.amsterdam/>
* [MWL Books](https://mwl.io)
* [Homura - Lutris like for FreeBSD](https://gitlab.com/Alexander88207/Homura)

## Japanese
* [Itazuraneko](https://itazuraneko.neocities.org/)
* [Japanese Quickstart](https://massimmersionapproach.com/table-of-contents/stage-1/jp-quickstart-guide/)
* [DJT Quizmaster](https://sites.google.com/view/audio-cards-guide/)
* [Tae Kim - Japanese](https://www.youtube.com/watch?v=t1hs-l68lOU&list=PL98F6E6BD58A09001&index=2)

### Other/Misc
* [Missing Semester](https://www.youtube.com/channel/UCuXy5tCgEninup9cGplbiFw)
* [rwxrob - Lots of great stuff](https://rwxrob.gg)
* [Ebook Foundation - Free Programming Books](https://github.com/EbookFoundation/free-programming-books/blob/master/free-programming-books.md)
* [linode - GPG Guide](https://www.linode.com/docs/security/encryption/gpg-keys-to-send-encrypted-messages/)
* [Image to text converter](https://convertio.co/ocr/japanese/)
* <https://browsersync.io/> - Daemon for browser development
* <https://hackaday.io/project/172292-introduction-to-reverse-engineering-with-ghidra>
* <https://github.com/chrismaltby/gb-studio>
* <http://gbdk.sourceforge.net/>
* [PS Hacking / Trinity Exploit](https://github.com/TheOfficialFloW)
* [How to Design Programs, Second Edition](https://htdp.org/2020-8-1/Book/index.html)
* [Unix Wars](https://www.livinginternet.com/i/iw_unix_war.htm)
* [Git Based Wiki](https://www.bit-101.com/blog/2020/09/git-based-wiki/)
* [Security Engineering - Third Edition](https://www.cl.cam.ac.uk/~rja14/book.html)
* [How Routers Work, Really](https://kamila.is//teaching/how-routers-work/)
* [3DS Hacking](https://3ds.hacks.guide/)
* [Wifi Cap](https://www.bettercap.org/)
* [Pwnagotchi](https://pwnagotchi.ai/)
* [Typing Practice / Classic Literature](https://www.typelit.io/)
* [macOS KVM Guide](https://github.com/foxlet/macOS-Simple-KVM)
